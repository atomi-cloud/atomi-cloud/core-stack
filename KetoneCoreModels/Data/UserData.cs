using System;
using System.Linq;

namespace KetoneCoreModels.Data
{
    public class UserData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string[] Label { get; set; }

        // For searching 
        public string[] LabelLower { get; set; }
    }
}