using System;
using System.Collections.Generic;

namespace KetoneCoreModels.Response
{
    public class UserResponse
    {
        public UserResponse(Guid id, string name, IEnumerable<string> label)
        {
            Id = id;
            Name = name;
            Label = label;
        }

        public Guid Id { get; }

        public string Name { get; }

        public IEnumerable<string> Label { get; }
    }
}