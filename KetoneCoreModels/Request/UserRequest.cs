using System.Collections.Generic;
using FluentValidation;

namespace KetoneCoreModels.Request
{
    public class UserRequest
    {
        public string Name { get; set; }

        public IEnumerable<string> Label { get; set; }
    }

    public class UserValidator : AbstractValidator<UserRequest>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name).NotNull();
            RuleForEach(x => x.Label).NotNull();
        }
    }
}