namespace KetoneCore
{
    internal static class ClientType
    {
        public const string Blobber = "blobber";
    }

    internal static class CorsPolicy
    {
        internal const string Development = "Development";
        internal const string Production = "Production";
    }

    internal static class BucketType
    {
        internal const string User = "User";
    }

    internal static class LabelType
    {
        internal const string User = "User";
    }
}