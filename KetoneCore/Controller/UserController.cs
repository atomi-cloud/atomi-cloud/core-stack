using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KetoneCore.Database;
using KetoneCoreModels.Data;
using KetoneCoreModels.Request;
using KetoneCoreModels.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KetoneCore.Controller
{
    public class User
    {
        public User(Guid id, string name, string[] label)
        {
            Id = id;
            Name = name;
            Label = label;
        }

        public Guid Id { get; }

        public string Name { get; }

        public string[] Label { get; }
    }

    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        public readonly CoreDbContext _db;

        public UserController(CoreDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserResponse>>> GetAll()
        {
            var r = await _db.Users.Select(x => new UserResponse(x.Id, x.Name, x.Label)).ToArrayAsync();
            return Ok(r);
        }

        [HttpPost]
        public async Task<ActionResult<UserResponse>> Create([FromBody] UserRequest request)
        {
            var user = new UserData
            {
                Label = request.Label.ToArray(),
                Name = request.Name,
            };

            var added = await _db.Users.AddAsync(user);
            await _db.SaveChangesAsync(); //this step
            var e = added.Entity;
            return new UserResponse(e.Id, e.Name, e.Label);
        }
    }
}