﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace KetoneCore.StatusManager
{
    [ApiController]
    [Route("")]
    public class MainController : ControllerBase
    {
        private readonly IServerStatusService _status;

        public MainController(IServerStatusService status)
        {
            _status = status;
        }

        [HttpGet]
        public async Task<ActionResult<ServerStatusResponse>> Get()
        {
            var status = await _status.Get();
            return Ok(new ServerStatusResponse(status));
        }
    }
}