using System;
using System.Linq;
using Kirinnee.Helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace KetoneCore
{
    public interface IGlobal
    {
        public bool AutoMigration { get; }
        public int DatabaseReconnectionAttempt { get; }

        public bool LocalEnv { get; }
        public string ConnectionString { get; }

        public string BlobPassword { get; }
        public string BlobPrivateEndpoint { get; }
        public string BlobPublicEndpoint { get; }

        public string[] AllowedOrigins { get; }
    }

    public class Global : IGlobal
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;

        public Global(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        public bool AutoMigration => _configuration["MIGRATE"]?.ToLower() == "auto";
        public int DatabaseReconnectionAttempt => int.Parse(_configuration["DB_CONNECT_TRIES"] ?? "50");
        public bool LocalEnv => _configuration["DEV_ENV"]?.ToLower() == "local";

        public string ConnectionString
        {
            get
            {
                if (_env.IsDevelopment())
                {
                    var key = LocalEnv ? "Local" : "Postgres";
                    return _configuration.GetConnectionString(key);
                }

                return _configuration["CONNECTION_STRING"] ??
                       throw new ApplicationException("No connection string specified");
            }
        }

        public string BlobPassword => _configuration["BLOBBER_PW"] ?? "";

        public string BlobPrivateEndpoint => _configuration["BLOBBER_PRIVATE"] ??
                                             throw new ApplicationException("No blobber connection string");

        public string BlobPublicEndpoint => _configuration["BLOBBER_PUBLIC"] ??
                                            throw new ApplicationException("No public url for blobber");

        public string[] AllowedOrigins => _configuration["CORS"]?.SplitBy(",").ToArray() ?? Array.Empty<string>();
    }
}