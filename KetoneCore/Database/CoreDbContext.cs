using KetoneCoreModels.Data;
using Microsoft.EntityFrameworkCore;

namespace KetoneCore.Database
{
#pragma warning disable 8618
    public class CoreDbContext : DbContext
    {
        public CoreDbContext(DbContextOptions<CoreDbContext> options) : base(options)
        {
        }

        public DbSet<UserData> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserData>()
                .Property(x => x.Name)
                .HasDefaultValue("Ernest");
        }
    }

#pragma warning restore 8618
}