﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Blobber;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace KetoneCore.Database
{
    public class BlobCheckHostedService : IHostedService
    {
        private readonly IGlobal _global;
        private readonly IHostApplicationLifetime _lifetime;
        private readonly HttpClient _client;

        public BlobCheckHostedService(IHttpClientFactory clientFactory, IGlobal global, IHostApplicationLifetime lifetime, HttpClient client)
        {
            _global = global;
            _lifetime = lifetime;
            _client = clientFactory.CreateClient(ClientType.Blobber);
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            var timeout = _global.DatabaseReconnectionAttempt;
            var tries = 0;
            var canConnect = false;
            while (!canConnect)
            {
                if (tries > timeout)
                {
                    Log.Fatal("Failed to blobber, exiting application");
                    Environment.ExitCode = 1;
                    _lifetime.StopApplication();
                    break;
                }

                Log.Information("Attempting to contact database: Attempt #{@int}", tries);
                try
                {
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(_global.BlobPrivateEndpoint)
                    };
                    using var response = await _client.SendAsync(request, cancellationToken);
                    response.EnsureSuccessStatusCode();
                    var r = await response.Content.ReadAsStringAsync(cancellationToken);
                    var s = r.To<BlobStatusResponse>();
                    if (s.Status == "Ok")
                        canConnect = true;
                    else
                        Log.Information("Cannot connect, status: {@string}", s.Status);
                }
                catch (Exception e)
                {
                    Log.Information("Cannot Connect: {@string}", e.Message);
                }

                Thread.Sleep(5000);
                tries++;
            }

            Log.Information("Contacted Blobber Successfully!");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}