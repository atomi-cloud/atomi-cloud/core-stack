using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace KetoneCore.Database
{
    public class DbMigratorHostedService : IHostedService
    {
        private readonly IGlobal _global;
        private readonly IServiceProvider _serviceProvider;
        private readonly IHostApplicationLifetime _lifetime;

        public DbMigratorHostedService(IServiceProvider serviceProvider, IGlobal global,
            IHostApplicationLifetime lifetime)
        {
            _serviceProvider = serviceProvider;
            _global = global;
            _lifetime = lifetime;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (!_global.AutoMigration)
            {
                Log.Information("Automatic programmatic migration is not enabled.");
                return;
            }


            using var scope = _serviceProvider.CreateScope();
            try
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<CoreDbContext>();
                var timeout = _global.DatabaseReconnectionAttempt;
                var tries = 0;
                var canConnect = false;
                while (!canConnect)
                {
                    if (tries > timeout)
                    {
                        Log.Fatal("Failed to contact DB, exiting application");
                        Environment.ExitCode = 1;
                        _lifetime.StopApplication();
                        break;
                    }

                    Log.Information("Attempting to contact database: Attempt #{@int}", tries);
                    try
                    {
                        canConnect = await dbContext.Database.CanConnectAsync(cancellationToken);
                    }
                    catch (Exception e)
                    {
                        Log.Information("Cannot Connect: {@string}", e.Message);
                    }

                    tries++;
                }

                Log.Information("Contacted Database Successfully!");

                await dbContext.Database.MigrateAsync(cancellationToken);
                Log.Information("Database migrated successfully.");
            }
            catch (Exception e)
            {
                Log.Fatal("Database migration failed: {msg}", e.Message);
                Log.Fatal(e.StackTrace);
                Environment.ExitCode = 1;
                _lifetime.StopApplication();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}