namespace KetoneCore.External
{
    public class LabelData
    {
        public int Id { get; set; }
        public string Key { get; set; }
        
        public string Discriminator { get; set; }
        
        public string Value { get; set; }
    }
}