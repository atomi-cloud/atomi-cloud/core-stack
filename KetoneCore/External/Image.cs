using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Kirinnee.Helper;

namespace KetoneCore.External
{
    /// <summary>
    /// Non safe-image type
    ///
    /// This data structure stores the "ID" of and image and the method to
    /// the public URL via environment variables
    /// </summary>
    public struct Image
    {
        private Image(string url)
        {
            Url = url;
        }


        public static Image Load(string url)
        {
            return new Image(url);
        }

        private static string TrimData(string input)
        {
            var m = Regex.Match(input, "^data:[a-z-]{1,20}/[a-z-]{1,20};base64,");
            return input.Skip(m.Value.Length);
        }

        /// <summary>
        /// Create an image from the base64 string.
        /// Automatically generate an ID for the image.
        /// </summary>
        /// <param name="blob">Base 64 string of the image</param>
        /// <param name="category">The category this is number (split it by bounded contexts)</param>
        /// <param name="blobService">The blob service used to save the image</param>
        /// <returns>Async Image</returns>
        public static async Task<Image> Create(string blob, string category,
            IBlobService blobService)
        {
            var data = TrimData(blob);
            var r = await blobService.Save(category, Guid.NewGuid().ToString(), data);
            return new Image(r.Link);
        }

        /// <summary>
        /// Create an image from the base64 string.
        /// </summary>
        /// <param name="blob">Base 64 string of the image</param>
        /// <param name="category">The category this is number (split it by bounded contexts)</param>
        /// <param name="id">The ID of the image</param>
        /// <param name="blobService">The blob service used to save the image</param>
        /// <returns></returns>
        public static async Task<Image> Create(string blob, string category, string id,
            IBlobService blobService)
        {
            var data = TrimData(blob);
            var r = await blobService.Save(category, id, data);
            return new Image(r.Link);
        }

        private string Url { get; }

        /// <summary>
        /// Unique ID of the image. Store this
        /// </summary>
        public string PrivateUrl => Url;


        /// <summary>
        /// Generate the public link for the image 
        /// </summary>
        /// <param name="service">Blob Service object used to calculate the public string of the object</param>
        /// <returns></returns>
        public string PublicUrl(IBlobService service)
        {
            return service.FormatUrl(Url);
        }
    }
}