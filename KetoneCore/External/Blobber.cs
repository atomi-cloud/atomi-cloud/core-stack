using System.Net.Http;
using System.Threading.Tasks;
using Common;
using Common.Blobber;
using Flurl;

namespace KetoneCore.External
{
    public interface IBlobService
    {
        Task<BlobResponseModel> Save(string dir, string name, byte[] content);

        Task<BlobResponseModel> Save(string dir, string name, string content);

        string FormatUrl(string blobUrl);
    }

    public class BlobService : IBlobService
    {
        private readonly HttpClient _client;
        private readonly IGlobal _global;

        public BlobService(IHttpClientFactory factory, IGlobal global)
        {
            _global = global;
            _client = factory.CreateClient(ClientType.Blobber);
        }


        public async Task<BlobResponseModel> Save(string dir, string name, byte[] content)
        {
            var endpoint = $"byte/{dir}/{name}";
            var res = await _client.PutAsync(endpoint, new ByteArrayContent(content));
            res.EnsureSuccessStatusCode();
            var value = await res.Content.ReadAsStringAsync();
            return value.To<BlobResponseModel>();
        }

        public async Task<BlobResponseModel> Save(string dir, string name, string content)
        {
            var endpoint = $"base64/{dir}/{name}";
            var res = await _client.PutAsync(endpoint, new StringContent(content));
            res.EnsureSuccessStatusCode();
            var value = await res.Content.ReadAsStringAsync();
            return value.To<BlobResponseModel>();
        }

        public string FormatUrl(string blobUrl)
        {
            return Url.Combine(_global.BlobPublicEndpoint, "files", blobUrl);
        }
    }
}