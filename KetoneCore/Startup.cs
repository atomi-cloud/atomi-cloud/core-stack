using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http.Headers;
using FluentValidation.AspNetCore;
using KetoneCore.Database;
using KetoneCore.StatusManager;
using KetoneCoreModels.Request;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace KetoneCore
{
    [ExcludeFromCodeCoverage]
    internal static class StartupExtensions
    {
        internal static IServiceCollection AddDatabase(this IServiceCollection services, IGlobal global)
        {
            services.AddDbContext<CoreDbContext>(options =>
                options.UseNpgsql(global.ConnectionString));
            return services;
        }

        internal static IServiceCollection AddHttpClients(this IServiceCollection services, IGlobal global)
        {
            services.AddHttpClient(ClientType.Blobber, c =>
            {
                c.BaseAddress = new Uri(global.BlobPrivateEndpoint);
                c.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", global.BlobPassword);
            });
            return services;
        }

        internal static IServiceCollection AddCorsPolicies(this IServiceCollection services, IGlobal global)
        {
            services.AddCors(options => options.AddPolicy(CorsPolicy.Development,
                builder => builder
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowAnyHeader()));

            services.AddCors(options => options.AddPolicy(CorsPolicy.Production,
                builder => builder
                    .AllowAnyHeader()
                    .AllowAnyHeader()
                    .WithOrigins(global.AllowedOrigins)));
            return services;
        }

        internal static IServiceCollection AddDomainService(this IServiceCollection services)
        {
            return services;
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        private IWebHostEnvironment Env { get; }
        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IGlobal global = new Global(Configuration, Env);
            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UserValidator>())
                .AddJsonOptions(options => { options.JsonSerializerOptions.IgnoreNullValues = true; });

            services.AddHttpContextAccessor();

            services
                .AddCorsPolicies(global)
                .AddHttpClients(global)
                .AddDatabase(global)
                .AddDomainService();
            
            services.AddHostedService<DbMigratorHostedService>();
            services.AddHostedService<BlobCheckHostedService>();

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo {Title = "KetoneCore", Version = "v1"}));
            services.AddSingleton<IServerStatusService, ServerStatusService>();
            services.AddSingleton<IGlobal, Global>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(CorsPolicy.Development);
            }
            else
            {
                app.UseCors(CorsPolicy.Production);
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "KetoneCore v1"));

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}