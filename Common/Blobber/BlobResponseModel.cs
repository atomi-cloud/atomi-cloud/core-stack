﻿#pragma warning disable 8618
namespace Common.Blobber
{
    public class BlobResponseModel
    {
        public string Link { get; set; }
        public string Mime { get; set; }
    }
}
#pragma warning restore 8618