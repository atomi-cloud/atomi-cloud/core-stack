﻿using System;

#pragma warning disable 8618
namespace Common.Blobber
{
    public class BlobStatusResponse
    {
        public string Status { get; set; }
        public DateTime Started { get; set; }
        public int RequestCount { get; set; }
    }
}
#pragma warning restore 8618