using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Humanizer;
using MimeDetective;

namespace Common
{
    public static class CustomValidators
    {
        public static IRuleBuilderOptions<T, IEnumerable<TElement>> MaxCollectionLength<T, TElement>(
            this IRuleBuilder<T, IEnumerable<TElement>> ruleBuilder, int max)
        {
            return ruleBuilder
                .Must(x => x.Count() <= max);
        }

        public static IRuleBuilderOptions<T, IEnumerable<TElement>> MinCollectionLength<T, TElement>(
            this IRuleBuilder<T, IEnumerable<TElement>> ruleBuilder, int min)
        {
            return ruleBuilder
                .Must(x => x.Count() >= min);
        }

        public static IRuleBuilderOptions<T, IEnumerable<TElement>> CollectionLength<T, TElement>(
            this IRuleBuilder<T, IEnumerable<TElement>> ruleBuilder, int min, int max)
        {
            return ruleBuilder
                .Must(x =>
                {
                    var tElements = x as TElement[] ?? x.ToArray();
                    return tElements.Length >= min && tElements.Length <= max;
                });
        }

        public static IRuleBuilderOptions<T, string> IsJson<T>(
            this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.Must(x =>
            {
                try
                {
                    x.To<object>();
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        public static IRuleBuilderOptions<T, byte[]> MimeType<T>(
            this IRuleBuilder<T, byte[]> ruleBuilder, IEnumerable<string> mimes)
        {
            return ruleBuilder
                .Must(x => mimes.Contains(x.GetFileType().Mime))
                .WithMessage("Invalid file type. Accepted mime types: " + mimes.Humanize());
        }

        public static IRuleBuilderOptions<T, string> MimeType<T>(
            this IRuleBuilder<T, string> ruleBuilder, IEnumerable<string> mimes)
        {
            return ruleBuilder
                .Must(x => mimes.Contains(x.FromBase64().GetFileType().Mime))
                .WithMessage("Invalid file type. Accepted mime types: " + mimes.Humanize());
        }

        public static IRuleBuilderOptions<T, byte[]> MimeType<T>(
            this IRuleBuilder<T, byte[]> ruleBuilder, string mimes)
        {
            return ruleBuilder
                .Must(x => mimes == x.GetFileType().Mime)
                .WithMessage("Invalid file type. Accepted mime types: " + mimes.Humanize());
        }

        public static IRuleBuilderOptions<T, string> MimeType<T>(
            this IRuleBuilder<T, string> ruleBuilder, string mimes)
        {
            return ruleBuilder
                .Must(x => mimes == x.FromBase64().GetFileType().Mime)
                .WithMessage("Invalid file type. Accepted mime types: " + mimes.Humanize());
        }
        
        public static IRuleBuilderOptions<T, string> NameValid<T>(
            this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Length(2, 128)
                .WithMessage("Name has to be between 2 to 128 characters");
        }
        
        public static IRuleBuilderOptions<T, string> DescriptionValid<T>(
            this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Length(2, 2048)
                .WithMessage("Description has to be between 2 to 2048 characters");
        }

    }
}