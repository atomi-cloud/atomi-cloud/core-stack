using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Newtonsoft.Json;
using NodaTime;

namespace Common
{
    public static class Utility
    {
        public static Random Rand = new Random((int) SystemClock.Instance.GetCurrentInstant().ToUnixTimeTicks());


        public static string ToBase64(this byte[] data)
        {
            return BitConverter.ToString(data);
        }

        public static byte[] FromBase64(this string data)
        {
            return Convert.FromBase64String(data);
        }

        public static Dictionary<K, NV> MapVal<K, V, NV>(this Dictionary<K, V> d, Func<K, V, NV> map)
            where K : notnull where NV : notnull
        {
            return d.ToDictionary(
                pair => pair.Key,
                pair => map(pair.Key, pair.Value)
            );
        }

        public static Dictionary<K, NV> MapVal<K, V, NV>(this Dictionary<K, V> d, Func<V, NV> map)
            where K : notnull where NV : notnull
        {
            return d.ToDictionary(
                pair => pair.Key,
                pair => map(pair.Value)
            );
        }

        public static Dictionary<NK, V> MapKey<K, V, NK>(this Dictionary<K, V> d, Func<K, V, NK> map)
            where K : notnull where NK : notnull
        {
            return d.ToDictionary(
                pair => map(pair.Key, pair.Value),
                pair => pair.Value
            );
        }

        public static Dictionary<TNk, TV> MapKey<TK, TV, TNk>(this Dictionary<TK, TV> d, Func<TK, TNk> map)
            where TK : notnull
            where TNk : notnull
        {
            return d.ToDictionary(
                pair => map(pair.Key),
                pair => pair.Value
            );
        }

        public static Dictionary<T, TK> ChooseVal<TK, T>(this IEnumerable<T> c, Func<T, TK> valFunc)
            where TK : notnull
            where T : notnull

        {
            return c.ToDictionary(e => e, valFunc);
        }

        public static Dictionary<TK, T> ChooseKey<TK, T>(this IEnumerable<T> c, Func<T, TK> keyFunc)
            where TK : notnull

        {
            return c.ToDictionary(keyFunc, e => e);
        }

        public static Dictionary<TV, TK> Invert<TK, TV>(this Dictionary<TK, TV> o)
            where TV : notnull where TK : notnull
        {
            return o.ToDictionary(pair => pair.Value, pair => pair.Key);
        }

        public static T To<T>(this string s)
        {
            return JsonConvert.DeserializeObject<T>(s);
        }

        public static string Json<T>(this T t)
        {
            return JsonConvert.SerializeObject(t);
        }


        public static T To<T>(this JsonDocument jdoc)
        {
            using var stream = new MemoryStream();
            Utf8JsonWriter writer = new Utf8JsonWriter(stream, new JsonWriterOptions {Indented = true});
            jdoc.WriteTo(writer);
            writer.Flush();
            var s = Encoding.UTF8.GetString(stream.ToArray());
            return s.To<T>();
        }

        public static JsonDocument JsonDoc<T>(this T t)
        {
            return JsonDocument.Parse(t.Json());
        }

        public static T Random<T>(this IEnumerable<T> arr)
        {
            var a = arr.ToArray();
            return a[Rand.Next(a.Length - 1)];
        }
    }
}