using System.Net.Http;
using System.Text;

namespace Common
{
    public static class HttpUtility
    {
        public static HttpRequestMessage ToMessage(this object req, HttpMethod method, string endpoint)
        {
            var content = req.ToStringRequest();
            return new HttpRequestMessage(method, endpoint) {Content = content};
        }

        public static StringContent ToStringRequest(this object req)
        {
            return new StringContent(req.Json(), Encoding.UTF8, "application/json");
        }
    }
}