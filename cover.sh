#!/usr/bin/env bash

dotnet test --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=\"json,opencover\" /p:Threshold=70 /p:ThresholdStat=average \
    /p:Exclude=\"[*]*.Controllers?.*,[*]*.Data.*,[*]*.Requests?.*,[*]*.Responses?.*,[*]*.Services?.*,[*]*.Repository.*,[*]*.Database.*,[*]*.Migrations.*\"
mkdir -p .coverage 