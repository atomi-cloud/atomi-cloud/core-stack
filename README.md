# Project Ketone Core

Ketone Core is a the core server of the project Ketone, also known publicly 
as AtomiCloud, a SaaS platform that allows user to manage and deploy workloads
across multiple cloud platforms.

The core server includes multiple CS project files, each service providing 
individual functionality to the overall architecture.

The core responsibility of the each service 

### Core
- Users
- Projects
- Payment Method
- Subscription
- Credentials
- Authorization

## Set-up local Development
Local development is supported by narwhal + docker swarm

### Pre-requisite
- Git
- Docker Swarm (API Version > 1.40)
- [Narwhal](https://gitlab.com/kiringo/narwhal) (Version > 0.3.11)

### Setup HOST file on your machine
As Ketone uses a reverse proxy (traefik) to redirect multiple routes 
in the same port, we require local developers to map multiple localhost
domain back to localhost (127.0.0.1). Please add the following entries
to the host file of your operating system:

```
127.0.0.1 core.localhost
127.0.0.1 admin.localhost
127.0.0.1 files.localhost
127.0.0.1 traefik.localhost
```

### Ports usage and endpoint
In local development, all services are hosted through the 5050 port
via the reverse proxy, and each separate service are exposed on the 
ports 5051 onwards incrementally

### Starting the stack
To start the stack, simply run
```
nw up -a
```
or 
```
narwhal up -a 
```

# Services
This section contains the details of each service. This includes
on how they can be accessed via within the docker swarm or from
outside the docker swarm.

### Core
Core Service
- Public port: 5051
- Internal Domain name: core
- Reverse proxy: core.localhost:5050
- Home: http://core.localhost:5050
- API definition: http://core.localhost:5050/swagger

### Core Database
Postgres SQL for core service
- Public port: 5450
- Internal Domain name: core-db
- Internal Port: 5432
- Username: user
- Password: pw
- Database: postgres

Manage via adminer

### Adminer
Local DB administration tool

To access database, enter `Internal Domain name`, `Internal port`,
`username` and `password` into the web interface.

- Public port: 5800
- Internal port: 8080
- Internal domain name: adminer
- Reverse Proxy: http://admin.localhost:5050

### Blobber
Simple blob storage mechanism. Visit [blobber](https://gitlab.com/kirinnee/blobber) 
on how to query blobber (do not use for production)

- Internal port: 9000
- Internal domain name: blobber
- Public port: 5801
- Reverse Proxy: http://files.localhost:5050

### Traefik
The dashboard for the reverse proxy. 

- Internal port: 80
- Internal domain name: rp
- Public port: 5050
- Dashboard: http://traefik.localhost:5050