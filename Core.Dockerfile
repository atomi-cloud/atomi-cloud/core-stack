FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

COPY KetoneCore/KetoneCore.csproj src/KetoneCore/
COPY KetoneCoreModels/KetoneCoreModels.csproj src/KetoneCoreModels/
COPY Common/Common.csproj src/Common/
RUN dotnet restore src/KetoneCore/KetoneCore.csproj
COPY KetoneCore/ src/KetoneCore/
COPY Common/ src/Common/
COPY KetoneCoreModels/ src/KetoneCoreModels/
WORKDIR src
RUN dotnet build "KetoneCore/KetoneCore.csproj" -c Release 

FROM build AS publish
RUN dotnet publish "KetoneCore/KetoneCore.csproj" -c Release -o /app

FROM mcr.microsoft.com/dotnet/aspnet:5.0-alpine AS final
RUN apk add curl
WORKDIR app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "KetoneCore.dll"]
