#!/usr/bin/env bash

narwhal cp -c src/Core -f MigrationDockerfile /src/migrate.sql ops/artifacts/migrations/core.sql
narwhal cp -c src/Math -f MigrationDockerfile /src/migrate.sql ops/artifacts/migrations/math.sql
narwhal cp -c src/Analytics -f MigrationDockerfile /src/migrate.sql ops/artifacts/migrations/analytics.sql            