#!/usr/bin/env bash
envsubst < ./ops/artifacts/deploy/env/ci-sub.yml > ./ops/artifacts/deploy/env/ci.yml
envsubst < ./ops/artifacts/deploy/env/images-sub.yml > ./ops/artifacts/deploy/env/images.yml
